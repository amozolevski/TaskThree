/*Задача №3
Написать консольное многопоточное приложение используя язык программирования Java
для проверки доступности другого сетевого устройства (виртуальной машины, другого компьютера в сети или сайта).

Требования:
1. Программа должна работать в 3-х потоках.
2. В консоль выводятся результат проверки в формате
(Thread#N --- TimeStamp ---- Статус проверки доступности <>); N - номер потока.
3. Интервал между выводами результатов 5 секунд.
4. Программа работает 1 минуту и завершает работы выводом сообщения об окончании работы.
5. Не обезательно но будет плюсом. Добавить функционал по управлению приоритетами потоков.
 */

public class Application {

    public static void main(String args[]){

        ConnectTester ct1 = new ConnectTester("http://www.google.com", 1);
        Thread thread1 = new Thread(ct1);

        ConnectTester ct2 = new ConnectTester("http://www.fortest.url", 2);
        Thread thread2 = new Thread(ct2);

        ConnectTester ct3 = new ConnectTester("http://www.yahoo.com", 3);
        Thread thread3 = new Thread(ct3);

        setThreadPriority(thread1, args, 0);
        setThreadPriority(thread2, args, 1);
        setThreadPriority(thread3, args, 2);

        thread1.start();
        thread2.start();
        thread3.start();

        try{
            Thread.sleep(60000);
            thread1.interrupt();
            thread2.interrupt();
            thread3.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

/*  Добавить функционал по управлению приоритетами потоков.
    args[0] - thread1 priority
    args[1] - thread2 priority
    args[2] - thread3 priority

    priority values:
    0 - MIN_PRIORITY
    1 - NORMAL_PRIORITY
    2 - MAX_PRIORITY
*/

    private static void setThreadPriority(Thread thread, String[] args, int value){

        int valueInt = -1;

        if (args.length > value){

            try{
                valueInt = Integer.parseInt(args[value]);
            } catch (NumberFormatException e) { }

            switch (valueInt){
                case 0:
                    thread.setPriority(Thread.MIN_PRIORITY);
                    break;
                case 1:
                    thread.setPriority(Thread.NORM_PRIORITY);
                    break;
                case 2:
                    thread.setPriority(Thread.MAX_PRIORITY);
                    break;
                default:
                    thread.setPriority(Thread.NORM_PRIORITY);
                    break;
            }
        } else {
            thread.setPriority(Thread.NORM_PRIORITY);
        }
    }
}