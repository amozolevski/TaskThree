import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class ConnectTester implements Runnable{

    public String address;
    public int threadNumber;

    public ConnectTester(String address, int threadNumber) {
        this.address = address;
        this.threadNumber = threadNumber;
    }

    public void run() {
        System.out.printf("Thread#%s --- <started...> \n", threadNumber + " --- " + timeStamp());

        while (!Thread.currentThread().isInterrupted()) {
            try{
                Long startTime = System.currentTimeMillis();

//                String checkResult = isURLReachable1();
                String checkResult = isURLReachable2();

                System.out.println("Thread#" + threadNumber + " --- " + timeStamp() + " --- <" + address + checkResult);
                Long actualTime = System.currentTimeMillis();

                //5 seconds without time for check connection method
                Long sleepTime = 5000 - (actualTime - startTime);
                if (sleepTime > 0) {
                    Thread.sleep(sleepTime);
                }
            }
            catch(InterruptedException e){
                break;
            }
        }
        System.out.printf("Thread#%s --- <finished...> \n", threadNumber + " --- " + timeStamp());
    }

    //the first method to check connection
    private String isURLReachable1() {
        String result = "";
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) new URL(address).openConnection();
            con.setRequestMethod("HEAD");
            result = (con.getResponseCode() == HttpURLConnection.HTTP_OK) ? " is reachable>" : " isn't reachable>" ;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    //the second method to check connection
    private String isURLReachable2() {
        try {
            URL url = new URL(address);

            //open a connection to that source
            HttpURLConnection urlConnect = (HttpURLConnection)url.openConnection();

            //trying to retrieve data from the source. If there
            //is no connection, this line will fail
            Object objData = urlConnect.getContent();
        }
        catch (UnknownHostException e) { return " isn't reachable>"; }
        catch (IOException e) { return " isn't reachable>"; }

        return " is reachable>";
    }

    private String timeStamp(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH.mm.ss");
        Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
        return sdf.format(timeStamp);
    }
}